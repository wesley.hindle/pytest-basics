# Flags
## -k
The `-k`  flag allows you to only run tests which contain a keyword:

By running the below you will only run the first test, as that matches the specified keyword
```bash
pytest -k abc examples/flags/test_flag_k.py
```

By running the below you will only run the second test, as that matches the specified keyword
```bash
pytest -k xyz examples/flags/test_flag_l.py
```

---

## -s
The `-s` flag shows any print statements in your tests in stdout

By running the below you are able to see any `print` statements.
```bash
pytest -s examples/flags/test_flag_s.py
```

---

## -v -vv
The `-v` or `-vv` flag will output verbose / very verbose test outputs.

By running the below you are able to see a slightly verbose error message:
```bash
pytest -v examples/flags/test_flag_vv.py
```

By running the below you can see a more verbose error message
```bash
pytest  -vv examples/flags/test_flag_vv.py
```