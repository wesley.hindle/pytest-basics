a = {
    'admins':
    {'admin.user1':
     {'name': 'Admin 1', 'squad': 'ID Crowd',
      'location': 'Sheffield',
      'email': 'admin.user1@engineering.digital.dwp.gov.uk',
      'exclude_from_lambda': True},
     'admin.user2':
     {'name': 'Admin 2', 'squad': 'FraggleRock',
      'location': 'Sheffield',
      'email': 'admin.user2@engineering.digital.dwp.gov.uk'}
     },
    'dev_ops':
    {'devops.user1':
     {'name': 'Devops 1', 'squad': 'Central',
      'location': 'Sheffield',
      'email': 'devops.user1@engineering.digital.dwp.gov.uk'},
     'devops.user2':
     {'name': 'Devops 2', 'squad': 'Shared',
      'location': 'Sheffield',
      'email': 'devops.user2@engineering.digital.dwp.gov.uk',
      'exclude_from_lambda': True}
     }
    }

b = {
    'admins':
    {'admin.user1':
     {'name': 'Admin 1',
      'squad': 'ID Crowd',
      'location': 'Sheffield',
      'email': 'admin.user1@engineering.digital.dwp.gov.uk',
      'exclude_from_lambda': True},
     'admin.user2':
     {'name': 'Admin 2',
      'squad': 'FraggleRock',
      'location': 'Sheffield',
      'email': 'admin.user2@engineering.digital.dwp.gov.uk',
      'exclude_from_lambda': False}},
    'dev_ops':
    {'devops.user1':
     {'name': 'Devops 1',
      'squad': 'Central',
      'location': 'Sheffield',
      'email': 'devops.user1@engineering.digital.dwp.gov.uk',
      'exclude_from_lambda': False},
     'devops.user2':
     {'name': 'Devops 2',
      'squad': 'Shared',
      'location': 'Sheffield',
      'email': 'devops.user2@engineering.digital.dwp.gov.uk',
      'exclude_from_lambda': True}
     }
}


def test_flag_vv():
    assert a == b