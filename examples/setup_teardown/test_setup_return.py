import pytest

@pytest.fixture
def test_fixture():
    x = "Hello world"
    yield x

def test_first_word(test_fixture):
    assert test_fixture == "Hello world"

def test_second_word(test_fixture):
    assert test_fixture is not "Goodbye world"
