import pytest

@pytest.fixture
def test_first_fixture():
    return "First fixture"

@pytest.fixture
def test_second_fixture(test_first_fixture):
    return_value = f"{test_first_fixture}, second fixture"
    return return_value

def test_output_two_fixtures(test_second_fixture):
    assert test_second_fixture == "First fixture, second fixture"