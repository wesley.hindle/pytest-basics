from cap import log_error, log_info, print_f
import logging

def test_log_info(caplog):
    with caplog.at_level(logging.INFO):
        log_info()
    assert "Useful log" in caplog.text


def test_log_error_fail(caplog):
    with caplog.at_level(logging.ERROR):
        log_error()
    assert "Info" not in caplog.text

def test_log_error_valid(caplog):
    with caplog.at_level(logging.ERROR):
        log_error()
    assert "Error log" in caplog.text

def test_print_simple(capsys):
    print_f()
    captured = capsys.readouterr()
    assert "tribute" in captured.out

def test_print_show_output(capsys):
    print_f()
    captured = capsys.readouterr()
    print(captured.out)
    assert "tribute" in captured.out