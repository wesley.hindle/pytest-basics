# Capturing Output
You may want to test that something is produced on stdout such as `print` statements, or `logs` that are prodced.

---

## Logs
Python log levels:
- Critical
- Error
- Warning
- Info
- Debug

When setting the log level, each level also includes all levels below it.

---

[Capturing logs](https://docs.pytest.org/en/7.1.x/how-to/logging.html#caplog-fixture) is straightforward, you should ensure that the text you're checking for is unique to the log you want to test.

```python
import logging

def test_function():
    with caplog.set_level(logging.<LOG-LEVEL-IN-CAPS>)
        function_to_test()
    assert "log text" in caplog.text
```


Basic logging test:
```python:
def test_log_info(caplog):
    with caplog.at_level(logging.INFO):
        log_info()
    assert "Useful log" in caplog.text
```

```bash
pytest -k test_log_info examples/cap_output/test_cap.py
```


When capturing logs, if you have multiple log levels in the same function, only the level you set to be captured will be captured.

This tests that only the error level log is captured:
```python
def test_log_error_fail(caplog):
    with caplog.at_level(logging.ERROR):
        log_error()
    assert "Info" not in caplog.text
```

```bash
pytest -k test_log_error_fail examples/cap_output/test_cap.py
```

And then double checking the above, by ensuring the `error log` *is* captured.
```python
def test_log_error_valid(caplog):
    with caplog.at_level(logging.ERROR):
        log_error()
    assert "Error log" in caplog.text
```

```bash
pytest -k test_log_error_valid examples/cap_output/test_cap.py
```

---

# Print
Print statements go to `stdout` and you can use the [capsys library](https://docs.pytest.org/en/7.1.x/how-to/capture-stdout-stderr.html#accessing-captured-output-from-a-test-function) to capture what is output there. You can also capture `stderr` output with the same library

The format for this test is straightforward. Note the function needs to be declared before `capsys`

```python
def test_function(capsys):
    function()
    captured = capsys.readouterr()
    assert "<print term>" in captured.out
```

A simple test for this:
```python
def test_print(capsys):
    print_f()
    captured = capsys.readouterr()
    assert "tribute" in captured.out
```

```bash
pytest -s -k test_print_simple examples/cap_output/test_cap.py
```

Or if you want to see the `stdout` that has been captured use:

```bash
pytest -s -k test_print_show_output examples/cap_output/test_cap.py
```