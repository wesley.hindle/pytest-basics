# Patching Environment Variables

You can use monkeypatching to patch the value's of env vars. A full list of what you can set using monkeypatching can be found [here.](https://docs.pytest.org/en/7.1.x/how-to/monkeypatch.html)

The format of this will look like:

You must import the whole module to be able to patch env vars.
```python
from unittest.mock import patch
import <module-name>
```

Add monkeypatch as a parameter to your test
```python
def function_name(monkeyname):
```

Specify which monkeypatch method you want to use. `.setitem()` is only used for setting dictionaries.
```python
monkeypatch.setitem()
```

Specify what you want to patch. Here you're specifying the module name, specify __dict__, the key you want to target and the value you want to assign to that key.

Env vars are treated as `key: value` pairs
```python
monkeypatch.setitem(<module-name>.__dict__, <env-var>, <value to patch>)
```

Actual example:
```python
monkeypatch.setitem(env_var.__dict__, "ENVIRONMENT", "TEST")
```

This put into a whole test would look like:

```python
def test_whats_my_age(monkeypatch):
    monkeypatch.setitem(env_var.__dict__, "ENVIRONMENT", "TEST")
    assert get_environment() == "TEST"
```
---

You can run the below to see the env var patching:
```bash
pytest -s -k test_whats_my_age examples/env_vars/test_env_var.py
```

And the below to see the test without env var patching:

```bash
pytest -s -k test_no_patch examples/env_vars/test_env_var.py
```