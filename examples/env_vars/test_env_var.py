import pytest 
from env_var import whats_my_age_again
import env_var
from unittest.mock import patch

def test_whats_my_age(monkeypatch):
    monkeypatch.setitem(env_var.__dict__, "AGE", "23")
    print(whats_my_age_again())
    assert whats_my_age_again() == "Nobody likes you when you're 23"

def test_no_patch():
    print(whats_my_age_again())
    assert whats_my_age_again() == "What's my age again? What's my age again?"