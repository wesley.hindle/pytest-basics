import os
AGE = os.environ.get("AGE", "30")

def whats_my_age_again():
    if AGE == "23":
        return ("Nobody likes you when you're 23")
    else:
        return ("What's my age again? What's my age again?")