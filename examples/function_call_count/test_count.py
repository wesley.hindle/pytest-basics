from count import og_function
from unittest.mock import patch

def test_show_total_basic():
    with patch("count.show_total") as mock_show_total:
        og_function()
        assert mock_show_total.call_count == 1

def test_show_total_two_funcs():
    with patch("count.show_total") as mock_show_total:
        with patch("count.show_total_again") as mock_show_total_again:
            og_function()
            assert mock_show_total.call_count == 1
            assert mock_show_total_again.call_count == 1

def test_show_total_mock_param():
    with patch("count.show_total") as mock_show_total:
        mock_show_total.return_value == 4
        with patch("count.show_total_again") as mock_show_total_again:
            og_function()
            assert mock_show_total.call_count == 1
            assert mock_show_total_again.call_count == 1
