# Counting Function Calls
You may need to test a function that calls a subsequent function. You're not interested in what the subsequent function does, only that it is called. For this you can use the `call_count` method.

The generic format for this test is:
```python
from unittest.mock import patch
from module import function_name

def test_function_name()
    with patch("module.function_name") as mock_function_name:
        function_name()
        assert mock_function_name.call_count == 1
```

See this in action by testing this code

```python
def og_function():
    total = 2+2
    same_total = show_total(total)
    show_total_again(same_total)
```

And the test checks that the `show_total` function has been called once.
```python
pytest -k test_show_total_basic examples/function_call_count/test_count.py
```

---

Your code to test may call multiple functions, and you can do the same as above, but nest the two mocked function calls.

```python
def test_function_name()
    with patch("module.function_name1") as mock_function_name:
        with patch("module.function_name2") as mock_function_name:
            function_name()
            assert mock_function_name1.call_count == 1
            assert mock_function_name2.call_count == 1
```

You can test this using:
```bash
pytest -k test_show_total_two_funcs examples/function_call_count/test_count.py
```

---

And you may need to check a function is called that required a parameter from an earlier function call. For this you can call different methods on mocked functions.

```python
def test_function_name()
    with patch("module.function_name1") as mock_function_name:
        mock_function_name.return_value == 2
        with patch("module.function_name2") as mock_function_name:
            function_name()
            assert mock_function_name2.call_count == 1
```

You can see this by running
```bash
pytest -k test_show_total_mock_param examples/function_call_count/test_count.py
```
