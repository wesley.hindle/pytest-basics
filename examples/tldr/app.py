import os
import logging
import yaml
import sys
from yaml.loader import SafeLoader
import random
import boto3


####################
# Context Managers #
####################
def rand_generator():
    x = random.randint(1,6)
    x += 1
    return(x)

def rand_int():
    x = rand_generator()
    y = x + 1
    return(y)

rand_int()


def first_name():
    return("Lafayette")


def middle_name():
    return("Ron")


def surname():
    return("Hubbard")


def full_name():
    f = first_name()
    m = middle_name()
    s = surname()

    fn = (f'{f} {m} {s}')
    return(fn)

####################
# Capturing Output #
####################

def log_info():
    logging.info("Useful log")

def log_error():
    logging.info("Info log")
    logging.error("Error log")

def print_f():
    print("This is a tribute, to the greatest print statement in the world.")


#####################
# Patching Env Vars #
#####################

AGE = os.environ.get("AGE", "30")

def whats_my_age_again():
    if AGE == "23":
        return ("Nobody likes you when you're 23")
    else:
        return ("What's my age again? What's my age again?")


###########################
# Counting Function Calls #
###########################

def og_function():
    total = 2+2
    same_total = show_total(total)
    show_total_again(same_total)

def show_total(total):
    print(total)
    return(total)

def show_total_again(total):
    print(total)


##################
# Monkeypatching #
##################

def my_name():
    x = input("What is your name?")
    return x


########################
# Parametrised Testing #
########################

def num_limit(a, b):
    total = a + b

    if total > 10:
        return ("Over 10")
    else:
        return ("Below 10")


######################
# Testing for Errors #
######################

def load_yaml_file(yaml_file):
    with open(yaml_file) as f:
        return yaml.load(f, Loader=SafeLoader)

def get_user(username):
    iam = boto3.client("iam")
    try:
        iam.get_user(UserName=username)
    except Exception as e:
        logging.error(f"Unable to retrieve user: {username}")

def sys_exit():
    if not os.environ.get("TEST_ENV_VAR"):
        sys.exit(1)


