import os
import logging
import random
import yaml
from yaml.loader import SafeLoader
import boto3
import logging
import pytest
import random
import sys
from unittest import mock
from unittest.mock import patch
from app import rand_int, full_name, log_error, log_info, print_f, \
    whats_my_age_again, og_function, my_name, num_limit, load_yaml_file, \
    get_user, sys_exit

import app

####################
# Context Managers #
####################

# Test to patch returned values in a function call
def test_random_int():
    # Patch the value where the function is CALLED not where it is used
    with patch("app.rand_generator") as mocked_get:
        mocked_get.return_value = 3

        y = rand_int()
        assert y == 4


# Test to patch multiple returned values in a function call
def test_full_name():
    with patch("app.surname") as mocked_get3:
        mocked_get3.return_value = "Grey"
        with patch("app.first_name") as mocked_get:
            mocked_get.return_value = "Gandalf"
            with patch("app.middle_name") as mocked_get2:
                mocked_get2.return_value = "the"

                f = full_name()
                assert f == "Gandalf the Grey"


####################
# Capturing Output #
####################

def test_log_info(caplog):
    # Only check logs at INFO level
    with caplog.at_level(logging.INFO):
        log_info()
    assert "Useful log" in caplog.text


def test_log_error_fail(caplog):
    # Only check logs at ERROR level
    with caplog.at_level(logging.ERROR):
        log_error()
    assert "Info" not in caplog.text

# Captures the log
def test_log_error_valid(caplog):
    with caplog.at_level(logging.ERROR):
        log_error()
    assert "Error log" in caplog.text

# Captures stdout
def test_print_simple(capsys):
    print_f()
    captured = capsys.readouterr()
    assert "tribute" in captured.out

def test_print_show_output(capsys):
    print_f()
    captured = capsys.readouterr()
    print(captured.out)
    # Asserts text in stdout
    assert "tribute" in captured.out


#####################
# Patching Env Vars #
#####################

# Import monkeypatch as a param
def test_whats_my_age(monkeypatch):
                        # Module,    env var, desired value
    monkeypatch.setitem(app.__dict__, "AGE", "23")
    print(whats_my_age_again())
    assert whats_my_age_again() == "Nobody likes you when you're 23"

def test_no_patch():
    print(whats_my_age_again())
    assert whats_my_age_again() == "What's my age again? What's my age again?"


###########################
# Counting Function Calls #
###########################


def test_show_total_basic():
    with patch("app.show_total") as mock_show_total:
        og_function()
        assert mock_show_total.call_count == 1


# Multiple function call counts are nested
def test_show_total_two_funcs():
    with patch("app.show_total") as mock_show_total:
        with patch("app.show_total_again") as mock_show_total_again:
            og_function()
            assert mock_show_total.call_count == 1
            assert mock_show_total_again.call_count == 1


# Combining patching return values and counting function calls
def test_show_total_mock_param():
    with patch("app.show_total") as mock_show_total:
        mock_show_total.return_value == 4
        with patch("app.show_total_again") as mock_show_total_again:
            og_function()
            assert mock_show_total.call_count == 1
            assert mock_show_total_again.call_count == 1


##################
# Monkeypatching #
##################

def test_my_name(monkeypatch):
    monkeypatch.setattr("builtins.input", lambda _: "Antony")
    assert my_name() == "Antony"


########################
# Parametrised Testing #
########################

# Set params as a fixture
# Assign the names of the params to be used and then add
# Them as elements in a list when you write the actual test
@pytest.mark.parametrize("a, b, expected", [
    (1, 1, "Below 10"),
    (5, 5, "Below 10"),
    (8, 8, "Over 10")
])

def test_num_limit(a, b, expected):
    assert num_limit(a, b) == expected


######################
# Testing for Errors #
######################

# Testing a specific error is raised
def test_load_yaml_file_not_found_error():
    with pytest.raises(FileNotFoundError):
        load_yaml_file("")


# Testing any error is raised
def test_load_yaml_file_exception():
    with pytest.raises(Exception):
        load_yaml_file("")


# Testing the code exits as you want it to
def test_sys_exit():
    with pytest.raises(SystemExit):
        sys_exit()

####################
# Setup / Fixtures #
####################

# Use the @pytest.fixture decorator to setup test conditions beforre tests
@pytest.fixture
def test_first_fixture():
    return "First fixture"

# You can build on previous test fixtures
@pytest.fixture
def test_second_fixture(test_first_fixture):
    return_value = f"{test_first_fixture}, second fixture"
    return return_value

def test_output_two_fixtures(test_second_fixture):
    assert test_second_fixture == "First fixture, second fixture"


@pytest.fixture
def test_fixture():
    x = "Hello world"
    yield x

def test_first_word(test_fixture):
    assert test_fixture == "Hello world"

def test_second_word(test_fixture):
    assert test_fixture is not "Goodbye world"