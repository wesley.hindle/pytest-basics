from mp import my_name

def test_my_name(monkeypatch):
    monkeypatch.setattr("builtins.input", lambda _: "Antony")
    assert my_name() == "Antony"