# Monkeypatching

Monkeypatching is how we can change things during run time. To use monkeypatching, it needs to be passed as a parameter into the consuming test.

[Full list of monkeypatch methods](https://docs.pytest.org/en/7.1.x/how-to/monkeypatch.html)

---
## Input Variables

Pass the parameter into the test
```python
def test_my_name(monkeypatch):
```

Configure how you want monkeypatching to work:

The monkeypatch method
```python
monkeypatch.setattr
```

The built ins library in python
```python
("builtins.
```

The builtin method
```python
input",
```

A lambda is an unnamed function, `_` is the var name, "Antony" is the var's value
```python
lambda _: "Antony")
```
Complete expression
```python
monkeypatch.setattr("builtins.input", lambda _: "Antony")
```

Full test to set the input as "Antony"
```python
def test_my_name(monkeypatch):
    monkeypatch.setattr("builtins.input", lambda _: "Antony")
    assert my_name() == "Antony"
```

You can test this using:
```bash
pytest -k my_name examples/monkeypatching/test_mp.py
```
