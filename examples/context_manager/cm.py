import random

def rand_generator():
    x = random.randint(1,6)
    x += 1
    return(x)

def rand_int():
    x = rand_generator()
    y = x + 1
    return(y)

rand_int()

###

def first_name():
    return("Lafayette")

def middle_name():
    return("Ron")

def surname():
    return("Hubbard")

def full_name():
    f = first_name()
    m = middle_name()
    s = surname()

    fn = (f'{f} {m} {s}')
    return(fn)
