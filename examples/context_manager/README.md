When using a context manager you are setting up the conditions requiried for the test. It is a way of `arranging`

When you are using patching as a context manager you are **patching the value where the function is called not where it is declared.**

```python
# Function is declared here
def rand_generator():
    x = random.randint(1,6)
    x += 1
    return(x)

def rand_int():
        # Patching happens here
    x = rand_generator()
    y = x + 1
    return(y)
```

You can see this in action by running:
```bash
pytest -k random_int examples/context_manager/test_cm.py
```

---

If you have code that uses the return values of multiple function calls then you can nest these patching statements.

```python
with patch("cm.surname") as mocked_get3:
        mocked_get3.return_value = "Grey"
        with patch("cm.first_name") as mocked_get:
            mocked_get.return_value = "Gandalf"
```

You can see this in action by running:
```bash
pytest -k full_name examples/context_manager/test_cm.py
```

---
