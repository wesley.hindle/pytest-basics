import pytest
from unittest import mock
from unittest.mock import patch

from cm import rand_int, full_name

def test_random_int():
    with patch("cm.rand_generator") as mocked_get:
        mocked_get.return_value = 3

        y = rand_int()
        assert y == 4

###

def test_full_name():
    with patch("cm.surname") as mocked_get3:
        mocked_get3.return_value = "Grey"
        with patch("cm.first_name") as mocked_get:
            mocked_get.return_value = "Gandalf"
            with patch("cm.middle_name") as mocked_get2:
                mocked_get2.return_value = "the"


                f = full_name()

                assert f == "Gandalf the Grey"

###