def div(a,b):
    print(a / b)


#div(10,0)

def div(a,b):
    try:
        print(a / b)
    except Exception:
        print("An error occurred")

#div(10,0)

def div(a,b):
    try:
        print(a / b)
    except ZeroDivisionError:
        print("You cannot divide by 0")

div(10,0)