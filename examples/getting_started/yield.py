import time

def large_memory(number):
    iterator = number
    return_list = []
    print(number)
    for i in range(iterator):
        return_list.append(i)

    yield return_list

def number_list():
    return_value = large_memory(333333)
    for each in return_value:
        print(each)


start_time = time.time()
number_list()
print("The return keyword function took \n--- %s seconds --- \n to run" % (time.time() - start_time))