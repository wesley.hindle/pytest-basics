# Testing for errors

If you're unfamiliar with `Exceptions` then please read the section on the `README` in root first.

Writing tests to check errors are raised is straight forward:

```python
import pytest
from module import my_function

def function_name():
    with pytest.raises(ErrorType):
        my_function()
```

You can write a less specific test to catch any error (except syntax):
```python
def function_name():
    with pytest.raises(Exception):
        my_function()
```

Or you can write tests to catch specific errors
```python
def function_name():
    with pytest.raises(NoSuchEntityError):
        my_function()
```

---


If you run the below you will see a `FileNotFoundError` error (Exception) is raised:
```bash
python3 examples/raises/raises_exe.py
```

We can write a test to to assert that *any* error (Exception) is produced:

```python
def test_load_yaml_file_empty_string():
    with pytest.raises(Exception):
        load_yaml_file("")
```

```bash
pytest -k exception examples/raises/test_raises.py
```


Or you can write a test to specifically check for `FileNotFoundError` which is produced here.

```python
def test_load_yaml_file_not_found_error():
    with pytest.raises(FileNotFoundError):
        load_yaml_file("")
```

```bash
pytest -k file_not_found_error examples/raises/test_raises.py
```
---

## Testing system exit
Your code may have validation rules to exit the code on certain condition.

To check that your code exits correctly you would use

```python
with pytest.raises(SystemExit)
```

You can test this by running
```bash
pytest -k test_sys_exit examples/raises/test_raises.py
```

---

## Test the log, not the error
If you're unsure about writing tests that check logs see `cap_output` first.

While a lot of APIs implement specific classes for error handling these can be difficult to use alongside `pytest.raises`. However, you should also be logging when errors occur. Instead of checking than an error has been raised, you can check a log has been produced instead.

We have this code we want to test:
```python
def get_user(username):
    iam = boto3.client("iam")
    try:
        iam.get_user(UserName=username)
    except Exception as e:
        logging.error(f"Unable to retrieve user: {username}")
```

Boto does have a [class for error handling](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/error-handling.html), however this can be tricky to setup and does not cover all possible errors.

Instead we can check that a log is produced rather than the error, since for a log to be produced an error (Exception) will have had to have been raised.

Here we're checking that part of the log is the captured log, doubling up and checking than an error (Exception) has been produced.
```python
def test_load_yaml_file_log(caplog):
    with caplog.at_level(logging.DEBUG):
        get_user("test.user1")
    assert "Unable to retrieve" in caplog.text
```
