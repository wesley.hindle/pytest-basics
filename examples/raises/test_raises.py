import os
import pytest
from raises import load_yaml_file, get_user, sys_exit
import boto3
import logging


def test_load_yaml_file_not_found_error():
    with pytest.raises(FileNotFoundError):
        load_yaml_file("")


def test_load_yaml_file_exception():
    with pytest.raises(Exception):
        load_yaml_file("")


def test_load_yaml_file_log(caplog):
    with caplog.at_level(logging.DEBUG):
        get_user("test.user1")
    assert "Unable to retrieve" in caplog.text

def test_sys_exit():
    with pytest.raises(SystemExit):
        sys_exit()
