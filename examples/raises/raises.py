import yaml
from yaml.loader import SafeLoader
import boto3
import logging
import sys
import os


def load_yaml_file(yaml_file):
    with open(yaml_file) as f:
        return yaml.load(f, Loader=SafeLoader)

def get_user(username):
    iam = boto3.client("iam")
    try:
        iam.get_user(UserName=username)
    except Exception as e:
        logging.error(f"Unable to retrieve user: {username}")

def sys_exit():
    if not os.environ.get("TEST_ENV_VAR"):
        sys.exit(1)

get_user("test.user")