import yaml
from yaml.loader import SafeLoader

def load_yaml_file(yaml_file):
    with open(yaml_file) as f:
        return yaml.load(f, Loader=SafeLoader)

load_yaml_file("")