from param import num_limit
import pytest

@pytest.mark.parametrize("a, b, expected", [
    (1, 1, "Below 10"),
    (5, 5, "Below 10"),
    (8, 8, "Over 10")
])

def test_num_limit(a, b, expected):
    assert num_limit(a, b) == expected
