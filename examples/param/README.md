# Parametrised test
[Parametrising tests](https://docs.pytest.org/en/6.2.x/example/parametrize.html#paramexamples) is a way to give a single test multiple data sets to test. This can keep your tests DRY and easier to read / maintain.

Writing parametrised uses fixtures and follows this format:


Setup your imports and then add this linem the boilerplate for these tests
```python
import pytest

@pytest.mark.parametrize("", [])
```

Here `p1` `p2` are the names of the parameters which will be passed into the code you want to test. `expected_value` is the value you expect based on the parameters and is used as part of the `assertion`
```python
@pytest.mark.parametrize("p1, p2, expected_value", [])
```

The parameters you want use in your tests are added as elements in the list
```python
@pytest.mark.parametrize("p1, p2, expected_value", [
    #param1   param2   expected value

    ("hello", "world", "hello world),
    ("hello", "sunshine", "hello sunshine)
])
```

An example of this would be if you wanted to test the below code you would need to test with parameters that:
- Total less than 10
- Total more than 10
- Total 10

And rather than have three tests, you can just write one parameterised test

```python
def num_limit(a, b):
    total = a + b
    if total > 10:
        return ("Over 10")
    else:
        return ("Below 10")
```

You can test the above using:
```bash
pytest -k test_num_limit examples/param/test_param.py
```
