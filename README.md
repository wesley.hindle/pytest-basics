# Pytest Basics

This repo will aim to give you a basic guide to unit testing in Python using the Pytest library. It will briefly touch on some common concepts behind unit testing and is intented only to get you started with testing, not be a comprehensive guide to doing so. It will not cover test-driven development.

Pytest builds on top of the included [unittest library](https://docs.python.org/3/library/unittest.html) and you are able to use its functions within `Pytest`. Pytest simplifies test writing compares to `unittest` by handling a lot of the boilerplate code behind-the-scenes.

This guide makes the following assumptions:
- You are comfortable with writing Python code and are aware of its common features
- Have some familiarity with testing
- You are not using a venv (there should be no difference if you are)

If you just want to get straight to the tests skip down to the examples section below.

---

## Getting Started
Pre requisities:
- Brew installed
- Pip installed
- Python3 installed >3.8



You can check you're setup correctly running the passing test with:
```python
pytest examples/getting_started/test.py
```

When test and app files are in the same directory no additional config is needed. If they are in separate files you need to add a blank `__init__.py` file into the app repo, so the test file can treat the app file as a module to be imported.

```bash
├── tests
│   └── test_app.py
├──__init__.py
└── app.py
```

Running `pytest` will also create `pycache`, `.pytest_cache` files, so you should add them dir to your `.gitignore` to prevent noisy pushes.

---
## TL;DR
If you just want to get started and see tests running, go to the `examples/tldr` directory, read through the code in both files and then run:

```bash
pip install boto3 pytest pyyaml &&
pytest examples/tldr/test_app.py
```

Also read the `README` in `examples/flags` to make running your tests easier.

---
## Arrange, Act, Assert
Tests are written around assertions, unit tests are written arround 'Arrange, Act, Assert'.

For each test you need to do 3 things:
- *Arrange* the necessary conditions for each test to run
- *Act* out the actions needed to run the code you want to test
- *Assert* that you get an expected outcome based on the conditions you used

Arranging the conditions for your test depends on numerous factors such as the complexity of the code you're testing, if you need to run your test in a stale environment (e.g. a database with only one record) etc.

Acting out the conditions is usually straight forward and is usually a function call of some sort.

Asserting your expected outcomes depends on what you're testing. You can have multiple assertions and a full list of them can be found [here.](https://docs.python.org/3/library/unittest.html#assert-methods)

Arrange, Act, Assert won't always follow that sequence. Some `assertion` methods require the `act` to be within the function call of the `assertion`.

---

A basic example of using arrange, act, assert:
```python
#Arrange
1 = "cat"
2 = "dog"

#Act:
1 = type(1)   #string
2 = type(2)   #string

#Assert:
assert 1 == 2  # True
```

A more complex test, but it still follows the arrange, act, assert template:

```python
def test_key_mgmt_delete_user_valid(iam_client):
    # Arrange
    username = "test.user"
    iam_client.create_user(Path='/staff/', UserName=username)
    iam_client.create_login_profile(
        UserName = username,
        Password = "Password1",
        PasswordResetRequired = False)
    iam_client.create_access_key(UserName=username)

    key_id = iam_client.list_access_keys(UserName=username)["AccessKeyMetadata"][0]["AccessKeyId"]
    keys_to_be_deleted = [{"aws_username": username, "access_key_id": key_id, "email": "test.user1@email.com"}]

    # Assert key exists before function call
    assert key_id != None

    # Act
    key_mngmt(keys_to_be_deleted, iam_client)

    # Assert the key has been deleted after use
    assert iam_client.list_access_keys(UserName=username)["AccessKeyMetadata"] == []
```

---
## Naming
Pytest runs files in the format `test_*.py` or `*_test.py`. Your tests should begin with `test_`.

When naming your tests using the format `test_<-function name being tested>_<whats being tested>` is a good choice e.g.

```python
# Code to test
def square_function(n):
    return n*n
```

```python
# Tests
def test_square_function_valid():
    assert square_function(5) == 25

def test_square_function_empty_param():
    pytest.raises(TypeError)
        square_function()
```

---

## Decorators and Generators
These are not exclusive to testing, but used heavily within them are decorators and generators.

### Decorators
[Decorators](https://docs.python.org/3/glossary.html#term-decorator) a way to extend what a function can do without amending the function itself.

While you may not need to write decorators for your tests, testing / mocking libraries do use them.

This function multiplies two *numbers* together and return the result. However, if you have one number and one string it will return multiple repetitions of that string.

So you want to add some validation without changing the `multiply_numbers` function itself

```python
# Original function
def multiply_numbers(a, b):
    return a*b
```

```python
# Modified original function with validation
def multiply_numbers(a, b):
    if a != int or b != str:
        return "One of the params is not an integer"

    return a*b
```

```python
# Original function, with decorator for validation
def validate(func):
    def rule(a, b):
        if a != int or b != int:
            print("One of the params is not a number")
            return
        func()
    return rule

@validate
def multiply_numbers(a, b):
    return a*b
```


#### Writing a decorator:

This parameter is always `func` and refers to your original function.
```python
def decorator_name(func)
```

This is the extended functionality you actually want to add to your existing function, which is a nested function.
```python
    def extended functionality():
```

Then you add the extended functionality you want, which can be before and / or after the function call
```python
    def extended functionality():
        # Extended functionality here
        # Call to original function
        # Extended functionality here
```

You need to add in a return call to the nested function
```python
    return extended_functionality
```

Complete template:
```python
def decorator_name(func)
    def extended_functionality():
        # Extended functionality here
        orig_func()
        # Extended functionality here
    return extended_functionality

@decorator_name
def orig_func():
    print("Hello")
```

We can then apply this template to take this meaningless interaction function:
```python
def valid_rule():
    print("Who's there?")
    print("Joe")
    print("Joe who?")
```

And turn it into an unfunny joke:
```python
def joke(func):
    def make_joke():
        print("Knock, knock")
        func()
        print("Joe mama")
    return make_joke

@joke
def valid_rule():
    print("Who's there?")
    print("Joe")
    print("Joe who?")
```

Which will return:
```bash
Knock, knock
Who's there?
Joe
Joe who?
Joe mama
```

---

### Generators
When calling a function you often will use a `return` keyword. This store the entire object in memory and pass it into the calling function, giving sub-optimal performance. An alternative to that is to use a `generator`, via the `yield` keyword.

A [generator](https://docs.python.org/3/glossary.html#term-generator-iterator) within a function will pause execution of that function (compared to using `return`) and allow execution to continue at a later time. Allowing you to `yield` one result at a time, rather than all results at once, as is the case when using `return`.

The below code will use up a lot of memory, be slow to run and can be optimised because the whole return value is stored in memory. You can test this by running
```bash
python3 examples/getting_started/return.py
```

```python
def large_memory(number):
    iterator = number
    return_list = []
    for i in range(iterator):
        return_list.append(i)

    return return_list

def number_list():
    return_value = large_memory(333333)
    for each in return_value:
        print(each)
```

Instead you can use a generator, where you get the same result, but in a more memory-efficient and quicker way. You can test this using:
```bash
python3 examples/getting_started/yield.py
```
```python
def large_memory(number):
    iterator = number
    return_list = []
    print(number)
    for i in range(iterator):
        return_list.append(i)

    yield return_list

def number_list():
    return_value = large_memory(333333)
    for each in return_value:
        print(each)

number_list()
```

---
## Refactoring
Tests are not immune to needing refactoring and this will need to happen as you write more tests, you may find the new tests inadvertently also test what you had written a separate test for in the past.

```python
# Code to test
def del_user(username):
    iam_client = boto3.client("iam")
    try:
        iam_client.delete_user(username)
    except Exception as e:
        logging.error(f"Error: {e})
```

```python
# Test 1 -  Error is produced
def test_del_user():
    # Arrange - Invalid username
    username = "test.user"

    # Assert
    pytest.raises(Exception):
        # Act
        del_user(username)
```

```python
# Test 2 - log is produced
def test_del_user(caplog):
    # Arrange - Invalid username
    username = "test.user"

    # Assert
    with caplog.att_level(logging.ERROR)
        # Act
        del_user(username)
    assert "Error:" in caplog.text
```

Here `test 2` checks the log, but for a log to be produced, an exception must have been raised, so you can remove `test 1` as this is covered in `test 2`.

---

## Setup (Fixtures)

### Setup
A lot of tests will require the same `arrangements` to be taken before the test is run, resulting in a larger codebase which isn't DRY. Instead of this you can use `setup` and `teardown`, in pytest setup is handled using the `@pytest.fixture` decorator.

When using fixtures, you should pass the function name the fixture is used with as a parameter into the test. Depending on your circumstances you may want to use either the `return` or `yield` keyword.

Fixtures usually have the format:
```python
import pytest

@pytest.fixture
def test_fixture():
    x = "These are my test arrangements"
    return x
        # or
    yield x

def test_function(test_fixture):
    assert test_fixture == "These are my test arrangements"
```

---



Run the below example using:
```bash
pytest examples/setup_teardown/test_setup.py
```
```python
# Setup
@pytest.fixture
def test_fixture():
    # Arrange
    x = "Hello world"
    yield x
```

```python
# Tests
def test_first_word(test_fixture):
  # Assert # Act
    assert test_fixture == "Hello world"

def test_second_word(test_fixture):
  # Assert # Act
    assert test_fixture is not "Goodbye world"
```

You can have multiple assertions for use in your test suite and you can pass one fixture into another to build upon the previous one's functionality.

Test this by running:
```bash
pytest examples/setup_teardown/test_multiple_fixtures.py
```

```python
@pytest.fixture
def test_first_fixture():
    return "First fixture"

@pytest.fixture
def test_second_fixture(test_first_fixture):
    return_value = f"{test_first_fixture}, second fixture"
    return return_value

def test_output_two_fixtures(test_second_fixture):
    assert test_second_fixture == "First fixture, second fixture"
```

---

## Mocking, Patching, Context manager
Monkeypatching is where you change a value during runtime. It is useful in testing where you want to change a value, such as an input, env var, or file path. A full list of methods can be found [here](https://docs.pytest.org/en/7.1.x/how-to/monkeypatch.html)

You can find working examples in `examples/monkeypatching`

Mocking is a way to replace real objects with fake ones to allow for controlled, predicatable results for use within testing e.g. an API, DB query.

You can find working examples in `examples/mocking`

Context manager is a way of setting up and tearing down resources as a pre requisite to testing. See Setup and Teardown.

---

## Exceptions
[Exceptions](https://docs.python.org/3/tutorial/errors.html) refer to any `error` that is not a syntax error, despite this delination people use these terms interchangably within a Python context.

Within code you need to have code to gracefully handle errors that can occur, rather than them causing the code to crash. Errors occurring is to be expected and it is more realistic to approiately handle them, rather than try to add validation in to account for every possibile cause of an error.

The [Exception class](https://docs.python.org/3/library/exceptions.html#BaseException) is the parent class from which specific API's error handling classes are a child of. The Exception class has built in exceptions, such as `ZeroDivisionError` and all `errors` (except syntax errors) will be classed as an exception.

### Error Handling

The most common way to handle errors in Python is with a `try except` block:

```python
try:
    # code to execute
    # If this execution fails then...
except Exception as error:
    # what you want to do about the error
    # An Exception has been raised
```

The below example will produce a `ZeroDivisionError`

Running this code would cause the code to crash because you the code doesn't know what to do when this error is produced.
```python
def div(a,b):
    print(a / b)

div(10/0)
```


You can improve this code though by adding in a try except block for a any error (Exception)
```python
def div(a,b):
    try:
        print(a / b)
    except Exception:
        print("An error occurred")

div(10,0)
```
This will print `An error occurred` and the code will not crash.


You can be more specific with your error handling if you wish, allowing more more granual error handling.
```python
def div(a,b):
    try:
        print(a / b)
    except ZeroDivisionError:
        print("You cannot divide by 0")

div(10,0)
```

This will print `You cannot divide by 0` and the code will not crash.

### Filtering by error
Errors produced are often responses to API calls and that response is often assigned to a variable for use e.g. in logging.

```python
try:
    # code
except Exception as e:
    logging.error(e)
```

When reusing functions in your code the error produced may not be the same one. But depending on the error you may want to perform a different action than if a different error was produced.

With the below code you want to perform a specific action is the error code is `NoSuchEntity` then if the error code is anything else. You can handle this as you normally would.

```python
def get_user(username):
    iam = boto3.client("iam")
    try:
        iam.get_user(UserName=username)
    except Exception as e:
        if e.response["Error"]["Code"] == "NoSuchEntity":
            return (e.response["Error"]["Code"])
        else:
            return ("Generic error")
```

---

## Flags
Pytest allows you to use flags to extend it's functionality. A full list can be found [here](https://docs.pytest.org/en/7.1.x/reference/reference.html#command-line-flags) and you can combine flags.

Usage:
```bash
pytest <flag> path/to/file
```

Some of the most common ones are:
- `-s` Shows any print statements in stdout
- `-k` Allows you to only run tests which contain a keyword
    * e.g. `pytest -k delete_record test_.py` will run any tests whose name contains 'delete_record'
- `-v` Verbose output
- `-vv` Very verbose output


## Examples
To see examples of how to use common pytest functionality go to the `examples` dir and use the corresponding `README` for the desired functionality.
Code to run files assume you are in root. If you are new to pytest I would recommend to look at them in this order

Functionality examples:
- Flags
- Monkeypatching
- Context Manager
- Testing for errors being raised
- System Exit
- Capturing Output
- Function Call Counting
- Parameterised tests
- Setup / Teardown

---
## Common Issues
- Some test methods require you to import the whole module, rather than its functions
- When running tests, if the code you're testing calls a function in that code, that function call will be executed as well as the test.